<?php

class AnonymousController extends Controller {

    public function header() {
	$app = $this->app;
	$app->render('header.php',compact('app'));
    }

    public function footer() {
	$this->app->render('footer.php');
    }

    public function index(){
	$this->header();
	$this->app->render('homepage.php');
	$this->footer();
    }

    public function yopla(){
	$this->header();
	$this->app->render('arf.php');
	$this->footer();
    }

    public function affiche_item($id){
	$this->header();
	$this->app->render('aff_item.php', compact('id'));
	$this->footer();
    }

    public function insert_info(){
	$nom = $this->app->request->post('nom');
	$this->app->flash('info', "J'ai ajouté le nom « $nom »");
	$this->app->redirect($this->app->urlFor('root'));
    }
}
